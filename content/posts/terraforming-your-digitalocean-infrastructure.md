# Terraforming your Digitalocean infrastructure
I have recently had the pleasure of getting more intimate with Terraform, the goto tool for reigning-in your cloud infrastructure if you want to be pseudo-independent of a specific
cloud provider. My main encounter have been during office hours with AWS as the prime target, an interesting beast in itself since I have only used Cloudformation for this earlier
when spinning up serverless projects.

 At work I have the "benefit" of being presented an existing codebase built with terraform, being a quite large project it easily can get quite confusing when starting to get into
 multi tier cloud architecture and having to map out where all the different dependencies are within services. Quite the mouthful one could say.
 
 Usually I learn best by getting my hands dirty, which led me to take control of my own environments at Digitalocean, my favourite service for getting small projects up and running and stimulating my
 own OPS side since around 5 years back.
 
 
## What is Terraform?
In essence (to avoid going down a rabbit-hole), it's a tool with which you can take control of your cloud environments by writing code. Writing code is something atleast I'm comfortable with so that's a good thing!
It also allows you to keep a track of the environment state in different manners which makes it easier and safer to collaborate within teams both locally and remote. Once a state is achieved, any further changes we make will have a plan presented on what the state will look like after changes being applied. Neat, right?

Read more about this tool and follow instructions for installing it at: [https://terraform.io](https://terraform.io)


## Getting started
These steps assume that there would be infrastructure parts already present in our Digitalocean account that needs to be imported, if this werent the case then we could do the exact same things but instead ignore any parts where we would import and skip to the planning and applying stages.


### Bootstrapping the project
It feels good to have these things separate and nice from our application codebases so we will create a infrastructure repository with git to contain our terraform code.
```bash
mkdir infrastructure && cd infrastructure
git init
```

Myself, I like to have a clear structure so having a subfolder to work in for Digitalocean as a cloud provider seems like a neat thing.
```bash
mkdir digitalocean && cd digitalocean
```

Here we create a file called `main.tf` which we put the following content in:
```javascript
variable "do_token" {}

provider "digitalocean" {
  token = "${var.do_token}"
}
```
The `do_token` variable statement will help us inject our access token when running our terraform codebase which will allow it to make all the needed changes. Other than that the rest just configures everything needed for terraform to communicate with Digitalocean, one could see it as setting up a client.

Now we are ready for the interesting stuff!

### Mapping out what we have
*(Mostly doctl will be used for retrieving information about our current  infrastructure, hop over to the [doctl github page](https://github.com/digitalocean/doctl) and install it!)*

First we start with the largest parts, namely droplets. To see the ones we have a quick doctl command can be issued:
```bash
doctl compute droplet list
ID          Name        Public IPv4        Private IPv4    Public IPv6    Memory    VCPUs    Disk    Region    Image               Status    Tags
1234578     web1        xx.xx.xx.xx                                       1024      1        25      lon1      Ubuntu 16.10 x64    active    
1234579     web2        xx.xx.xx.xx                                       1024      1        25      lon1      Ubuntu 18.04 x64    active
```

Based on our findings here we need to write code for two droplets, which is quite similar and we can start from the same snippet for both. 
*Copy and paste the following right below the provider in our `main.tf`file.*
```javascript
resource "digitalocean_droplet" "www1" {
  image       = ""
  name        = "www1"
  region      = "lon1"
  size        = "s-1vcpu-1gb"
  resize_disk = false
}
```
